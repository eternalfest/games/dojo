package dojo;

import patchman.DebugConsole;
import patchman.Ref;
import patchman.IPatch;
import etwin.ds.FrozenArray;

@:build(patchman.Build.di())
class Dojo {
    public var initialFruits: Int;
    public var timerSprite: Dynamic;
    public var chronoSprite: Dynamic;

    @:diExport
    public var patches(default, null): FrozenArray<IPatch>;

    public function new() {
        var patches = [
            Ref.auto(hf.mode.GameMode.onBadsReady).before(function(hf: hf.Hf, self: hf.mode.GameMode): Void {
                initialFruits = self.getBadClearList().length;
            }),

            Ref.auto(hf.mode.GameMode.nextLevel).after(function(hf: hf.Hf, self: hf.mode.GameMode): Void {
                for (i in 0...self.getPlayerList().length) {
                    self.getPlayerList()[i].getScore(self.getPlayerList()[i], Math.floor(Std.parseInt(timerSprite.field.text) / self.getPlayerList().length));
                }
            }),

            Ref.auto(hf.mode.GameMode.switchDimensionById).before(function(hf: hf.Hf, self: hf.mode.GameMode, id: Int, lid: Int, pid: Int): Void {
                for (i in 0...self.getPlayerList().length) {
                    self.getPlayerList()[i].getScore(self.getPlayerList()[i], Math.floor(Std.parseInt(timerSprite.field.text) / self.getPlayerList().length));
                }
            }),
        ];
        this.patches = FrozenArray.from(patches);
    }
}
