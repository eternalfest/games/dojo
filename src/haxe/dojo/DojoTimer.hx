package dojo;

import patchman.IPatch;
import etwin.ds.FrozenArray;
import etwin.Obfu;
import bottom_bar.BottomBarInterface;
import bottom_bar.modules.BottomBarModule;

class DojoTimer extends BottomBarModule {
    public var dojo: Dojo;
    public var x: Int;

    public var patches(default, null): FrozenArray<IPatch>;

    public function new(module: Map<String, Dynamic>, dojo: Dojo) {
        this.dojo = dojo;
        this.x = module[Obfu.raw("x")];
    }

    public override function init(bottomBar:BottomBarInterface): Void {
        this.dojo.chronoSprite.removeMovieClip();
        this.dojo.chronoSprite = bottomBar.game.depthMan.attach(Obfu.raw('chrono'), bottomBar.game.root.Data.DP_TOP + 1);
        this.dojo.chronoSprite._xscale = 80;
        this.dojo.chronoSprite._yscale = 80;
        this.dojo.chronoSprite._x = this.x - 10;
        this.dojo.chronoSprite._y = 517;

        this.dojo.timerSprite.removeMovieClip();
        this.dojo.timerSprite = bottomBar.game.depthMan.attach('hammer_interf_inGameMsg', bottomBar.game.root.Data.DP_TOP + 1);
        this.dojo.timerSprite.field._visible = true;
        this.dojo.timerSprite.field._width = 400;
        this.dojo.timerSprite.field._xscale = 115;
        this.dojo.timerSprite.field._yscale = 115;
        this.dojo.timerSprite.field._x = this.x;
        this.dojo.timerSprite.field._y = 499;
        this.dojo.timerSprite.field.text = "0";
        this.dojo.timerSprite.label._visible = false;
        bottomBar.game.root.FxManager.addGlow(this.dojo.timerSprite, 7366029, 2);
    }

    public override function update(bottomBar: BottomBarInterface): Bool {
        if (!bottomBar.game.fl_lock && !bottomBar.game.fl_pause) {
            var temps = bottomBar.game.world.scriptEngine.cycle / bottomBar.game.root.Data.SECOND;

            var SCORE = Math.floor((60 - temps)*100)/100;
            SCORE = 500 * SCORE * Math.sqrt((this.dojo.initialFruits / 5 + 1) * (bottomBar.game.world.currentId / 50 + 1));
            SCORE = Math.max(1000, SCORE);
            SCORE = Math.min(99999, SCORE);
            SCORE = Math.floor(SCORE/100)*100;

            var text: String = Std.string(SCORE);
            var l: Int = 5 - text.length;
            for (i in 0...l) {
                text = '0' + text;
            }
            if (text != this.dojo.timerSprite.field.text) {
              this.dojo.timerSprite.field.text = text;
            }
        }
        return true;
    }
}
