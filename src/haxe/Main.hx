import dojo.Dojo;
import bugfix.Bugfix;
import debug.Debug;
import hf.Hf;
import merlin.Merlin;
import game_params.GameParams;
import bottom_bar.BottomBar;
import bottom_bar.modules.BottomBarModuleFactory;
import dojo.DojoTimer;
import patchman.IPatch;
import patchman.Patchman;
import etwin.Obfu;
import better_script.NoNextLevel;

@:build(patchman.Build.di())
class Main {
  public static function main(): Void {
    Patchman.bootstrap(Main);
  }

  public function new(
    bugfix: Bugfix,
    debug: Debug,
	gameParams: GameParams,
	noNextLevel: NoNextLevel,
    bottomBar: BottomBar,
    dojo: Dojo,
	merlin: Merlin,
    patches: Array<IPatch>,
    hf: Hf
  ) {
      BottomBarModuleFactory.get().addModule(Obfu.raw("DojoTimer"), function(data: Dynamic) return new DojoTimer(data, dojo));
    Patchman.patchAll(patches, hf);
  }
}
