# Le Dojo

Clone with HTTPS:
```shell
git clone https://gitlab.com/eternalfest/games/dojo.git
```

Clone with SSH:
```shell
git clone git@gitlab.com:eternalfest/games/dojo.git
```
